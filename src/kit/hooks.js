import React, { useEffect, useState } from "react";

export default function MyComponent(props) {
    const [name, setName] = useState("John");

    useEffect(() => {
        document.title = name;
    });

    function handleNameChange(e) {
        setName(e.target.value);
    }

    return (
        <section>
            <input>
                value={name}
                onChange={handleNameChange}
            </input>
        </section>
    )
}